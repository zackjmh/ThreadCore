 ADD_EXECUTABLE(ThreadCoreTest Test_Driver.cpp 
                               ThreadManagerTests.cpp
							   Test.h
                               ThreadCoreTest.h
							   JobCountTest.h)
 
 TARGET_INCLUDE_DIRECTORIES(ThreadCoreTest PUBLIC 
								ThreadCore 
								#Tests require TestKit to be present and accessible from the engine root
								${CMAKE_SOURCE_DIR}/catch2)

 TARGET_LINK_LIBRARIES(ThreadCoreTest PUBLIC 
							ThreadCore)

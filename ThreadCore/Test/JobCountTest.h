#pragma once
#ifndef TC_JOB_COUNT_TEST_H
#define TC_JOB_COUNT_TEST_H
#include "ThreadCoreTest.h"
#include <chrono>

namespace ThreadCore {
	namespace Test {

		using std::chrono::high_resolution_clock;
		using IH_Utils::LogLevel;

		class JobCountTest : public ThreadCoreTest {
		public:
			JobCountTest(uint64_t expected_) : expected {expected_}{

			}

			virtual void test() noexcept override final {
				Test_Logger("Starting job count test for expected value " + to_string(expected));
				tm.queue_job([](Worker_t w) {}, expected);

				do {
				} while (!tm.is_idle());

				sum = tm.sum();
			};

			virtual bool test_assert() noexcept override final {
				if (sum != expected) {
					Test_Logger("TEST FAILED: Sum was " + to_string(sum), LogLevel::CRITICAL);
					return false;
				} else {
					Test_Logger("TEST PASSED: Sum was " + to_string(sum), LogLevel::CRITICAL);
					return true;
				}
			};

			~JobCountTest() noexcept {

			}

		private:
			uint64_t expected;
			uint64_t sum = 0;
			
		};
	}
}
#endif // !TC_BASE_TEST_H

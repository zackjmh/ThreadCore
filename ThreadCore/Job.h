#pragma once
#ifndef JOB_ABST_H
#define JOB_ABST_H

#include <mutex>
#include <map>
#include <thread>
#include <memory>
#include <array>
#include <typeindex>
#include <SmallBuffer.h>
#include "Worker.h"

namespace ThreadCore {

	class Job : public IH_Utils::SmallBuffer {
		//Job interface
	public:

		Job() : vptr(nullptr), super() {};

		template <typename T, typename dtype = std::decay_t<T>>
		Job(T&& j) : vptr{ &vtable_for<dtype> }, super(std::forward<T>(j)){
		};

		~Job() noexcept {
		};

		inline void operator()(Worker_t worker) {
			while (worker.run_count()){
				worker();
				vptr->exec(&buffer, std::forward<Worker_t>(worker));
			} 
		}


	private:
		struct vtable;
		using vtable_t = vtable const* const;
		vtable_t vptr;

		struct vtable {
			void(*exec)(void* this_, Worker_t worker);
		};

		template <typename T>
		//type erasure internals
		static inline vtable const vtable_for = {
			[](void* this_, Worker_t worker) {
				(*static_cast<T*>(this_))(std::forward<Worker_t>(worker));
			},
		};

	};

	using Job_t = Job;
}
#endif

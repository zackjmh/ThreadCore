#include "ThreadManager.h"
#include <iostream>
#include <numeric>
#include <utility>

namespace chrono = std::chrono;

namespace ThreadCore {

	void calc_ops(uint64_t cur_jobs, uint64_t seen_jobs, chrono::nanoseconds duration) {
		float ops = (cur_jobs - seen_jobs) * (std::nano::den / static_cast<float>(duration.count()));
		if ((ops / std::mega::num) >= 1000) {
			printf("%f GOPS.\n", (ops / static_cast<float>(std::giga::num)));
		} else {
			printf("%f MOPS.\n", (ops / static_cast<float>(std::mega::num)));
		}
	}

	ThreadManager::ThreadManager() :
		MAX_THREADS(std::thread::hardware_concurrency()),
		job_pool(),
		thread_pool(MAX_THREADS),
		worker_pool(),
		start_time(chrono::high_resolution_clock::now()),
		next_dispatch_(0) {

		for (size_t i = 0; i < MAX_THREADS; ++i) {
			thread_pool[i] = std::thread([&]() {
				Worker worker;
				worker_pool.push_back(&worker);
				while (worker.alive()) {
					find_work(worker);
				}

			});
		}
	}

	class ThreadManager::Job_pool_d {
	public:

		Job_pool_d(Job_t&& j, const size_t MAX_THREADS) : pool_count(MAX_THREADS),
			pool_guards(MAX_THREADS),
			thread_count(MAX_THREADS),
			cur_thread(0),
			job_pool(MAX_THREADS, std::move(j)) {
		}

		inline void get_job(Worker_t worker) {
			for (size_t i = 0; i < thread_count * 8; ++i) {
				try_job(i, std::forward<Worker_t>(worker));
			}
		};


		inline bool try_job(size_t offset, Worker_t worker, bool to_run = true) {
			size_t cur = (offset + worker.worker_id) % thread_count;

			if (!pool_count[cur]) return false;

			{//lock scope
				std::unique_lock<std::recursive_mutex> lk(pool_guards[cur]);

				if (!lk.owns_lock()) {
					return false;
				}

				const auto queue = (pool_count[cur] / thread_count) + (pool_count[cur] % thread_count);
				worker.to_run(queue);
				pool_count[cur] -= queue;
				if (pool_count[cur] < std::mega::num * 375) {
					worker.to_run(pool_count[cur]);
					pool_count[cur] = 0;
				}
			}
			while (worker.run_count() < std::mega::num * 375) {
					if(!try_job(cur, std::forward<Worker_t>(worker), false)) break;
			}
			if(to_run)
				job_pool[cur](worker);
			assert(!to_run || worker.run_count() == 0);
			return true;

		}

		inline void add_job(uint64_t num_jobs = 1) {
			if (num_jobs >= thread_count) {
				const auto per = (num_jobs / thread_count);
				size_t index = 0;
				while (num_jobs > per) {
					size_t cur = index % thread_count;
					std::lock_guard<std::recursive_mutex> lk(pool_guards[cur]);
					pool_count[cur] += per;
					num_jobs -= per;
					++index;
				}
				std::unique_lock<std::recursive_mutex> lk(pool_guards[index % thread_count]);
				while (!lk.owns_lock()) {
					lk.lock();
				}
				pool_count[index % thread_count] += num_jobs;
			} else {
				std::unique_lock<std::recursive_mutex> lk(pool_guards[cur_thread]);
				while (!lk.owns_lock()) {
					lk.lock();
				}
				pool_count[cur_thread++] += num_jobs;
				if (cur_thread == thread_count) {
					cur_thread = 0;
				}
			}
		};

		inline uint64_t count() const {
			return std::accumulate(pool_count.begin(), pool_count.end(), uint64_t(0));
		}

		inline uint64_t count(size_t worker_id) const {
			return pool_count[worker_id];
		}

	private:
		std::vector<uint64_t>   pool_count;
		std::vector<std::recursive_mutex> pool_guards;
		const uint64_t          thread_count;
		size_t                  cur_thread;
		std::vector<Job_t>      job_pool;
	};

	inline void ThreadManager::find_work(Worker_t worker) {
		assert(worker.run_count() == 0);
		for (auto& j : job_pool) {
			//auto& j_pool = j.second;
			if (j.second.count()) {
				j.second.get_job(worker);
			}
		}
	}

	ThreadManager::~ThreadManager() {
		std::for_each(worker_pool.begin(), worker_pool.end(), [](Worker* worker) {
			//bring the threads to an end
			worker->kill();
		});

		std::for_each(thread_pool.begin(), thread_pool.end(), [](std::thread &thread) {
			//wait for all the threads to join
			thread.join();
		});

	}

	void ThreadManager::queue_job(Job_t j, uint64_t to_dispatch /*default 1*/) {
		//If we have a pool add the job, otherwise add an entry to the job pool then add the jobs
		if (job_pool.count(j.get_type())) {
			job_pool.at(j.get_type()).add_job(to_dispatch);
		} else {
			std::lock_guard<std::mutex> lk(pool_create_lock);
			if (!job_pool.count(j.get_type())) {
				job_pool.emplace(std::make_pair(j.get_type(), Job_pool_d{ std::forward<Job_t>(j), MAX_THREADS }));
				job_pool.at(j.get_type()).add_job(to_dispatch);
			} else {
				job_pool.at(j.get_type()).add_job(to_dispatch);
			}
		}
	}

	bool ThreadManager::is_idle() {
		//if a worker is executing or has a non empty queue, is_idle returns false
		bool ret = false;
		for (auto& a : job_pool) {
			ret = ret || a.second.count();
		}

		for(auto& worker : worker_pool) {
			ret = ret || worker->run_count();
		};

#ifndef NDEBUG
		frame_count(ret);
#endif
		return !ret;
	}

#ifndef NDEBUG
	inline void ThreadManager::frame_count(bool ret) {

		//Debug mode OPS counter
		auto dur = chrono::duration_cast<chrono::nanoseconds>(chrono::high_resolution_clock::now()
			- start_time);

		if (dur >= chrono::seconds(1) || (!ret && dur.count() != 0)) {

			uint64_t job_counter = sum();

		
			//Subtract off previously seen jobs, devide by dur.count() just in case it isn't exactly 1s
			printf("Running at ");
			calc_ops(job_counter, job_count, dur);
			//update seen job total, reset the lower timepoint to now
			job_count = job_counter;
			start_time = chrono::high_resolution_clock::now();
		}
	}
#endif

	uint64_t ThreadManager::sum(bool report) const {
		return std::accumulate(worker_pool.begin(), worker_pool.end(), uint64_t(0), [=](uint64_t in, Worker* worker) {
			return in + worker->job_count(report);
		});
	}

}

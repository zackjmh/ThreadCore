#define _CRT_SECURE_NO_WARNINGS 1 
#pragma once
#ifndef IH_LOGGER_H
#define IH_LOGGER_H
#include <fstream>
#include <string>
#include <chrono>
#include <iomanip>
#include <time.h>
#include <iostream>
#include <unordered_map>
#include <algorithm>

namespace IH_Utils {

	using std::ofstream;
	using std::flush;
	using std::endl;
	using std::string;
	using std::chrono::system_clock;
	using std::put_time;
	using std::tm;
	using std::time_t;
	using std::localtime;

	/*LogLevels: 
	     DETAIL        - Incidental details, messages of no concequence
		 WARNING       - Message indicates a potential, but non-serious issue
		                 App will likely function without issue in 95% of situations. Diagnostics suggested.
		 STERN_WARNING - Message issues potentially serious issue. 
		                 App may appear to work properly, but is malformed. Diagnostics is strongly suggested
		 CRITICAL      - Message indicates a serious issue that requires diagnostics. 
		                 App is unstable at best.
		 FATAL         - Message indicates that the app encountered an issue that forced it to terminate.
		                 Diagnostics required for app to function.*/

	enum class LogLevel { DETAIL, WARNING, STERN_WARNING, CRITICAL, FATAL };

	struct LogLevelList {
		LogLevel    level;
		const char* name;
	};

	constexpr LogLevelList ll_list[]{
		{ LogLevel::DETAIL,        "DETAIL"  },
		{ LogLevel::WARNING,       "WARNING" },
		{ LogLevel::STERN_WARNING, "STERN_WARNING" },
		{ LogLevel::CRITICAL,      "CRITICAL" },
		{ LogLevel::FATAL,         "FATAL" }};


	constexpr const char* value(LogLevel level, LogLevelList const *entries) {
		return entries->level == level ? entries->name : value(level, entries + 1);
	}
	

	class Logger {
	public:

		void operator()(const string&& rhs, LogLevel level_ = LogLevel::DETAIL, bool pre_line = false) {
			if (level_ >= level) {
				time_t tt = system_clock::to_time_t(system_clock::now());

				struct tm ptm{};
				localtime_s(&ptm, &tt);

				string timestr;
				timestr.resize(26);

				asctime_s(timestr.data(), timestr.size(), &ptm);

				timestr.erase(std::remove(timestr.begin(), timestr.end(), '\n'), timestr.end());

				if (pre_line)
					logfile_stream << '\n';

				logfile_stream << timestr << " " 
					<< value(level_, ll_list)  << " - "
					<< rhs << endl; //insert newline and flush
			}
		}

		static Logger& get_logger(string filename, LogLevel level = LogLevel::DETAIL) noexcept {
			static Logger inst{ filename, level };
			if (!inst.logfile_stream.is_open()) {
				inst.logfile_stream.open(filename, ofstream::out | ofstream::app);
				inst.logfile_stream << "------------------------------------------" << endl;
				inst("Begin new log.", LogLevel::DETAIL);
			}
			return inst;
		}

		~Logger() {
			(*this)("Logger exited normal.\n"); 
			logfile_stream.close();
		}

	private:

		Logger(string& filename, LogLevel level = LogLevel::DETAIL) :
			filename{ move(filename) }, 
			level { level } {
		}

		string   filename;
		ofstream logfile_stream;

		LogLevel level;

		Logger(const Logger&) = delete;
		Logger(const Logger&&) = delete;
		Logger& operator=(const Logger&) = delete;
		Logger& operator=(const Logger&&) = delete;
	};

}

#endif // !IH_LOGGER_H

#pragma once
#ifndef OVGF_GAME_H
#define OVGF_GAME_H
#include <vulkan/vulkan.hpp>
#include "OVGFWindow.h"
#include "OVGFVulkan.h"
#include "OVGFConfig.h"


namespace OVGF {

	using std::string;

	class OVGFApp {
	public:
		OVGFApp(string = "config/default.cog") noexcept; 
		~OVGFApp() noexcept {};


	private:
		OVGFSettings settings;
		OVGFWindow   window;
		OVGFVulkan   renderer;


		OVGFSettings load_config(string,string = "=");
	};
}
#endif
